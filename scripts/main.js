/// <reference path="libs/modernizr-2.6.1-dev.js" />
/// <reference path="libs/jquery-1.7.2-dev.js" />
/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 * Date: Tue Jan 31 12:00:00 2012 +0200
 * Modified: Tue Jun 19 16:00:00 2012 +0200
 */
require.config({
	paths: {
		'jquery': 'libs/jquery-1.7.2-min',
		'core': 'tools/core'
	}
});
require(
	[
		'jquery',
		'libs/modernizr',
		'core',
		'modules/imageseries',
		'modules/articlefigure',
		'modules/articlefront'
	],
	function (
		$,
		Modernizr,
		core,
		_ImageSeries,
		_ArticleFigure,
		_ArticleFront
	) {
		var $win = $(window);

		$(document).on('ready', function () {
			// Image series
			$('.imageseries').each(function() {
				var $elem = $(this);
				if ($elem.find('.imageseries-image').length) {
					new _ImageSeries($elem);
				}
			});
			// Article front
			$('.article-front').each(function() {
				new _ArticleFront($(this));
			});
			// Article figure
			$('.article-figure').each(function() {
				new _ArticleFigure($(this), true, true);
			});

			// Artificial love scrolling videos
			$('.artificiallove-video.is-scroll').each(function() {
				var $container = $(this),
				$video = $container.find('video'),
				video = $video[0],
				$page = $container.closest('.page'),
				pageOffset,
				offset,
				scrollTop,
				scroll,
				resize,
				scrollTimer,
				resetTimer,
				distance,
				rotation = 1000,
				animate;

				animate = function () {
					var end, pos;
					end = video.duration;
					pos = ( ( distance - ( Math.floor( distance / rotation ) * rotation ) ) / rotation ) * end;
					video.currentTime = pos;
				};
				
				scroll = function () {
					scrollTop = $win.scrollTop();
					distance = scrollTop - offset - (pageOffset - offset);
					if (scrollTop > offset) {
						$container.css('top', distance);
					} else {
						$container.css('top', 0);
					}
					animate();
				};
				resize = function () {
					pageOffset = $page.offset().top;
					offset = $container.offset().top;
				};
				$win.on('scroll', function (event) {
					clearTimeout(scrollTimer);
					scrollTimer = setTimeout(scroll, 200);
				});
				$win.on('resize', function (event) {
					clearTimeout(resetTimer);
					resetTimer = setTimeout(resize, 200);
				});
				resize();
				scroll();
			});
			
			// What to count on - tips
			$('.whattocounton-quote').hide();
			$('.whattocounton-tip').on('click', function (event) {
				event.preventDefault();
				var $target = $($(this).attr('href'));
				$target.slideToggle();
				setTimeout(function() {
					$('html,body').animate({scrollTop: $target.offset().top-40});
				},100);
			});
			
			// Imagine the registration scroll
			$('.imaginetheregistration-illustration').each(function () {
				var $container = $(this),
				$img = $container.children(),
				$content = $container.closest('.article-content'),
				steps = 11,
				height,
				parts,
				scrollTimer,
				resetTimer,
				resize,
				scroll,
				containerOffset,
				imgHeight,
				src = $img.data('src'),
				isAlreadyFixed = false,
				margin = 48;

				resize = function () {
					$img.width($container.width());
					containerOffset = $container.offset().top;
					imgHeight = $img.height();
					height = $content.height() + $content.offset().top - containerOffset;
					parts = height / steps;
				};
				$win.on('resize', function (event) {
					clearTimeout(resetTimer);
					resetTimer = setTimeout(resize, 200);
				});
				resize();

				scroll = function () {
					var scrollTop = $win.scrollTop(),
					distance = scrollTop - containerOffset,
					no = Math.ceil(distance / height * steps + 1);

					if (distance + margin > 0 && !isAlreadyFixed) {
						$img.addClass('is-fixed').css('top', margin);
						isAlreadyFixed = true;
					} else if (distance + margin < 0 && isAlreadyFixed) {
						$img.removeClass('is-fixed');
						isAlreadyFixed = false;
					}

					$img.attr('src', src.replace('#', no || 1));
				};
				$win.on('scroll', function (event) {
					//clearTimeout(scrollTimer);
					//scrollTimer = setTimeout(scroll, 200);
					scroll();
				});
				scroll();

			});
		});
	}
);
