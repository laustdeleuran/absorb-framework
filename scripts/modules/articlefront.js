/**
 * Article front
 *
 * Article front
 *
 * @section   Module
 * @author    ldeleuran
*/
define(['jquery', 'core', 'libs/jquery-smartresize-1.0-dev'], function($, core){

	var _ArticleFront = function () {
		return this.construct.apply(this, arguments);
	};
	_ArticleFront.prototype = (function() {
		var setupImage, // Private functions
		$window, // Private vars
		construct;// Public functions

		$window = $(window);

		construct = function ($container) {
			var scope = this;

			this.$container = $container;

			if ($container.data('image') !== false) {
				setupImage.apply(this);
			}
		};
		setupImage = function () {
			var scope = this,
			$img, fit;

			$img = scope.$container.find('img');

			fit = function () {
				var containerSize  = {
					height: scope.$container.height(),
					width: scope.$container.width()
				},
				imgSize = {
					height: $img.height(),
					width: $img.width()
				},
				ratio = imgSize.width / imgSize.height;
				$img.css({
					'min-height': containerSize.height,
					'min-width': containerSize.width,
					'width': containerSize.width / ratio > containerSize.height ? containerSize.width : containerSize.height * ratio
				});
			};
			$window.smartresize(fit);
			fit();
		};

		return {
			construct: construct
		};
	}());

	return _ArticleFront;
});