/**
 * Image series element
 *
 * Image series
 *
 * @section   Module
 * @author    ldeleuran
*/
define(['jquery', 'core'], function($, core){
	var $img, $desc, $descLink, $close, $canvas, $container, canvas, _ImageSeries, showDescription, hideDescription;

	_ImageSeries = function ($elem) {
		$container = $elem;
		$img = $container.find('.imageseries-image');
		$desc = $container.find('.imageseries-description');
		$descLink = $container.find('.imageseries-itemlink');
		$close = $container.find('.imageseries-description-close');
		
		if (!$img.length || !$desc.length || !$descLink.length || !$close.length) {
			core.error.write(false, '** imageseries **', 'Required elements not found');
			return false;
		}
		$container.addClass('is-enhanced');

		$descLink.on('click', function (e) {
			e.preventDefault();
			showDescription();
		});
		$close.on('click', function (e) {
			e.preventDefault();
			hideDescription();
		});

		return true;
	};

	showDescription = function () {
		var offsetY = Math.round( ( $img.height() - $desc.outerHeight() ) / 2 );
		$desc
			.css('top', offsetY)
			.addClass('is-active');
		$container.addClass('has-description');
	};
	hideDescription = function () {
		$desc.removeClass('is-active');
		$container.removeClass('has-description');
	};

	// Expose
	_ImageSeries.prototype.showDescription = showDescription;
	_ImageSeries.prototype.hideDescription = hideDescription;

	return _ImageSeries;
});