/**
 * Article figure
 *
 * Article figure
 *
 * @section   Module
 * @author    ldeleuran
*/
define(['jquery', 'core'], function($, core){

	var _ArticleFigure = function () {
		return this.construct.apply(this, arguments);
	};
	_ArticleFigure.prototype = (function() {
		var bind, detect, listen, setupVideo, // Private functions
		$window, // Private vars
		construct, show, hide;// Public functions

		$window = $(window);

		bind = function() {
			var scope = this, debounceScroll, debounceResize;

			$window.on('resize load', function (event) {
				clearTimeout(debounceResize);
				debounceResize = setTimeout(function () {
					detect.apply(scope, [event]);
				}, 100);
			});
			detect.apply(scope);

			$window.on('scroll', function (event) {
				clearTimeout(debounceScroll);
				debounceScroll = setTimeout(function () {
					listen.apply(scope, [event]);
				}, 100);
			});
			listen.apply(scope);
		};
		construct = function ($elem, toggle, autoplay) {
			var scope = this;

			scope.$markup = $elem;
			scope.toggle = !!toggle;
			
			scope.hide();

			bind.apply(scope);

			setupVideo.apply(scope, [autoplay]);

			return scope;
		};
		detect = function () {
			var scope = this;

			scope.offset = scope.$markup.offset();
			scope.size = {
				width: scope.$markup.outerWidth(),
				height: scope.$markup.outerHeight()
			};
			scope.viewport = {
				width: $window.width(),
				height: $window.height()
			};

			return scope;
		};
		hide = function () {
			var check = !this.$markup.is('.is-hidden');

			if (check) {
				this.$markup.addClass('is-hidden');

				/*if (this.$markup.$video && this.$markup.$video[0] && this.$markup.$video.autoplay) {
					try {
						this.$video[0].pause();
					} catch(err) {}
				}*/
			}

			return check;
		};
		listen = function (event) {
			var scope = this, scroll, threshold;

			if (!event) return false;

			threshold = 20;
			scroll = {
				top: $window.scrollTop(),
				left: $window.scrollLeft()
			};

			if (scope.offset.top + threshold <= ( scroll.top + scope.viewport.height ) &&
				scope.offset.left + threshold <= ( scroll.left + scope.viewport.width ) &&
				( scope.offset.top + scope.size.height ) - threshold >= scroll.top &&
				( scope.offset.left + scope.size.width ) - threshold >= scroll.left) {
				scope.show();
			} else if (scope.toggle) {
				scope.hide();
			}

			return scope;
		};
		show = function () {
			var check = this.$markup.is('.is-hidden');

			if (check) {
				this.$markup.removeClass('is-hidden');

				/*if (this.$markup.$video && this.$markup.$video[0] && this.$markup.$video.autoplay) {
					try {
						$video.on('canplay', function () {
							this.$video[0].play();
						});
						this.$video[0].play();
					} catch(err) {}
				}*/
			}

			return check;
		};
		setupVideo = function (autoplay) {
			var scope = this,
			$video;
			/*if (scope.$markup.hasClass('is-video')) {
				$video = scope.$markup.find('video');
				$video.autoplay = autoplay === false ? false : true ;
				if (!$video.autoplay) {
					$video.attr('controls', 'controls');
				}
				scope.$markup.$video = $video;
				return $video;
			} else {
				return false;
			}*/
		};

		return {
			construct: construct,
			hide: hide,
			show: show
		};
	}());

	return _ArticleFigure;
});